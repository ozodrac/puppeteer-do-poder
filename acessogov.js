const puppeteer = require('puppeteer');

const BASE_URL = "https://acesso.gov.br/";


let browser = null;
let page = null; 

const acesso = {
    init: async () =>{
        browser = await puppeteer.launch({
            headless: false,
        });
        page = await browser.newPage();
        await page.goto(BASE_URL);
    },
    login: async(username,password)=>{
        await page.waitFor("#j_username");
        await page.type("#j_username", username);
        await page.click('#kc-login');

        await page.waitFor('#j_password');
        await page.type('#j_password', password);
        await page.click('#submit-button');
    },
    end: async()=>{
        await browser.close();
    }
}
module.exports = acesso;